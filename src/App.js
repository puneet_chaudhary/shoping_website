import React from "react";
import  data  from "./data.json";
import Products from "./Component/Products";
import Cart from "./Component/Cart";
import Filter from "./Component/Filter";

class App extends React.Component {
   constructor(){
     super();
     this.state = {
       products : data.products,
       size:"",
       sort:"",
       cardItems:[]
     }
   }


   removeFromCart=(product)=>{
     
     const cartItems = this.state.cardItems.slice();
     
      this.setState({cartItems:cartItems.filter((x)=> x._id !== product._id),})
   }

   addToCart=(product) =>{
   
    let cardItems = this.state.cardItems.slice()
    let alreadyInCart = false
    cardItems.forEach((item) =>{
      if(item._id === product._id){
        item.count++
        alreadyInCart =  true

      }
    })
      if(!alreadyInCart){
        cardItems.push({...product,count:1})
      }
   
    this.setState({cardItems},()=>{
      localStorage.setItem("cartItems", JSON.stringify(this.state.cartItems));
    })
 
   }   
   sortProduct=(event)=>{
     let sort = event.target.value
     this.setState({
       sort:sort,
       products:
       this.state.products.slice().sort((a,b)=>
       sort === "lowest" ? a.price > b.price ? 1 : -1 : sort === "highest" ? a.price < b.price ? 1 : -1 : a._id > b._id ? 1 : -1 ), 
     })

   }
   filterProducts=(event)=>{
     
    let size = event.target.value
    if(size === ""){
      this.setState({size:size,products:data.products})
    } 
    else{
     this.setState({size:size,products: data.products.filter((product) => product.availableSizes.indexOf(size)>=0),});
    }

   }

   createOrder = (order)=>{
     alert("You need to save this order" + order.name)
   }
  render(){
  return (
    <div className="grid-container">
      <header>
      <a href="/">React Shopping Cart</a>
      </header>
      <main>
        <div className="content">
          <div className="main">
            <Filter count={this.state.products.length}
            size={this.state.size}
            sort={this.state.sort}
            filterProducts={this.filterProducts}
            sortProduct={this.sortProduct}
            ></Filter>
            <Products products={this.state.products}  addToCart={this.addToCart} />
          </div>
          <div className="sidebar">
          
            <Cart cartItems={this.state.cardItems} removeFromCart={this.removeFromCart} createOrder={this.createOrder}/>
          </div>
        </div>
      </main>
      <footer>
        React shopping Footer section
      </footer>
    </div>
  );
  }
}

export default App;


