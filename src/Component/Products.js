import React, { Component } from 'react'
import Fade from "react-reveal/Fade";
import Modal from "react-modal";
import Zoom from "react-reveal/Zoom"


export default class Products extends Component {
    constructor(){
        super()
        this.state={
            productItem: null,
            modal:false
        }
    }
    openModal = (product) =>{
        console.log("pppppppppp",product)
        this.setState({ productItem:product, modal:true })
    }

    closeModal = () =>{
        this.setState({productItem:null, modal:false})
    }
    render() {
        let product = this.props.products
        return (
            <div>
                <Fade bottom cascade>
                <ul className="products">
                    {
                        this.props.products.map(product =>{
                            return(
                            <li key={product._id}>
                                <div className="product">
                                    <a href={"#"+ product._id}
                                    onClick={() => this.openModal(product)}>
                                        <img src={product.image} alt={product.title} />
                        <p>{product.title}</p>
                                    </a>
                                    <div className="product-price">
                        <div>{product.price}</div>
                        <button className="button primary" onClick={()=> this.props.addToCart(product)}>Add to Cart</button>
                                    </div>
                                </div>
                            </li>
                            )
                        })
                    }
                </ul>
                </Fade>
                {
                    this.state.productItem && 
                    (<Modal isOpen={this.state.modal} onRequestClose={this.closeModal}>
                    <Zoom>
                        <button onClick={this.closeModal}>X</button>
                        <div className="product-details">
                       <img src={this.state.productItem.image} alt={product.title}>
                            </img>
                            <div className="product-details-description">
                                <p>
                <strong>{this.state.productItem.title}</strong>
                                </p>
              
                <p>{this.state.productItem.description}</p>
                 <p>
                    Available Sizes:{" "}
                    {
                        this.state.productItem.availableSizes.map((x)=>{
                            return(
                        <span>{" "}
                        
                        <button className="button">{x}</button>
                        
                        </span>
                            )
                        })
                    }
                </p>
                <div className="product-price">
                <div>Price:{" "}{this.state.productItem.price}</div>
                <button className="button primary" onClick={()=>{
                    this.props.addToCart(this.state.productItem);
                    this.closeModal();
                }}>Add To Cart</button>
                </div>
                            </div>
                        </div>
                       
                    </Zoom>
                    </Modal>)
                }
            </div>
        )
    }
}
