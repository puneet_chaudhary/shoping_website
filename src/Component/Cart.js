import React, { Component } from 'react' 
import Fade from "react-reveal/Fade";

export default class Cart extends Component {
    constructor(props){
        super(props)
        this.state={
            name:"",
            email:"",
            address:"",
          showCheckout:false
        }

    }

    handleInput = (e)=>{
        this.setState({[e.target.name]: e.target.value});
    }

    placeOrder = (e)=>{
        e.preventDefault();
        const order = {
            name:this.state.name,
            email:this.state.email,
            address:this.state.address,
            cartItems:this.props.cartItems
        }
        this.props.createOrder(order)
    }
    render() {
        let { cartItems } = this.props
        console.log("this is the carts item", cartItems)
        return (
            <div>
              {
                  cartItems.length ===  0 ? (<div className="cart cart-header">Cart is Empty</div>) :
              (<div className="cart cart-header">{cartItems.length} in the cart</div>)
              } 
              
              <div className="cart">
                  <Fade left cascade>
                  <ul className="cart-items">
                      {
                          cartItems.map((item)=>{
                              return(
                              <li key={item._id}>
                                  <div >
                                  <img src={item.image} alt={item.title}/>
                                  </div>
                                  <div>
                              <div>{item.title}</div>
                              <div className="right">
                                  {item.price} * {item.count}{" "}
                              </div>
                              <button onClick={()=> this.props.removeFromCart(item)}>
                                  Remove
                              </button>
                                  </div>
                              </li>
                              )
                          })
                      }

                  </ul>
                  </Fade>
             </div>
             {
                 cartItems.length !== 0 &&
             <div className="cart">
              <div className="total">
                  <div>
                      Total:{" "}
                      {cartItems.reduce((a,c)=> a + c.price * c.count,0)}
                  </div>
                  <button className="button primary" onClick={()=> this.setState({showCheckout:true})}>Proceed</button>
              </div>
             </div>

    }
    {
        this.state.showCheckout && (
            <div className="cart">
                <form>
                    <Fade right cascade>
                    <ul className="form-container">
                        <li>
                            <label>Name</label>
                            <input type="text"
                             name="name"
                             required
                             onChange={this.handleInput}>
                            </input>
                            <label>Email</label>
                            <input type="email"
                             name="email"
                             required
                             onChange={this.handleInput}>
                            </input>
                            <label>Address</label>
                            <input type="text"
                             name="address"
                             required
                             onChange={this.handleInput}>
                            </input>
                        </li>
                         <li>
                             <button className="button primary" type="sumbit" onClick={this.placeOrder}>Checkout</button>
                         </li>
                    </ul>
                    </Fade>
                </form>

            </div>
        )
    }
            </div>
        )
    }
}
